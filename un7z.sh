#!/bin/bash
shopt -s extglob nullglob
outdir="$HOME/下載/"
outinsidedir="./*"

passwordlist=(
 )

for filename in *.@(zip|rar|7z)*; do #find compressed files
  if [[ $filename != *part* ]]; then #standalone file
    for passwd in "${passwordlist[@]}"; do
      if 7z x -y -p"$passwd" "$filename" -aoa -o"$outinsidedir"; then
        break
      fi
    done
  elif  [[ $filename == *.@(part01|part1|7z.001|zip.001)* ]]; then #part files
    for passwd in "${passwordlist[@]}"; do
      if 7z x -y -p"$passwd" "$filename" -aoa -o"$outinsidedir"; then
        break
      fi
    done
  else break
  fi
done
